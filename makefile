SHELL := /bin/bash

include config.mk

PWD=$(shell pwd)
BUILD_DIR=${PWD}/build
SRC_DIR=${PWD}/src
STATUS=0
EXP_BUILD_DIR=${BUILD_DIR}
ARTEFACTS_PATH=./artefacts

export ERE_INFRA_LOC
export INFRA
export EXP_BUILD_DIR

build: clean create-build-dir copy-html copy-artefacts copy-ere write-artefacts-path

all: install-infra build 

create-build-dir:
	mkdir -p ${BUILD_DIR}

copy-html:
	rsync -a ${SRC_DIR}/exp.html ${BUILD_DIR}/

copy-ere:
	(cd ${CATALOGS_DIR}/${CATALOG}; make -k copy-ere)

clean:
	rm -rf ${BUILD_DIR}

run:
	(cd build; python2 -m SimpleHTTPServer)

write-artefacts-path:
	echo 'var ARTEFACTS_URL="${ARTEFACTS_PATH}";' >> ${BUILD_DIR}/js/artefacts-path.js

copy-artefacts:

	(cd ${BUILD_DIR}; git clone https://gitlab.com/vlead-projects/experiments/ds/red-black-tree/artefacts.git; cd $(ARTEFACTS_PATH); make all )
